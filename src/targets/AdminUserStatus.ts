import { UserNotFoundError } from "../errors";
import { Services } from "../services";

interface Input {
    UserPoolId: string;
    Username: string;
}

export type ConfirmForgotPasswordTarget = (body: Input) => Promise<{}>;

export const AdminDisableUser = ({
      cognitoClient,
  }: Services): ConfirmForgotPasswordTarget => async (body) => {
    const userPool = await cognitoClient.getUserPool(body.UserPoolId);
    const user = await userPool.getUserByUsername(body.Username);

    if (!user) {
        throw new UserNotFoundError();
    }

    await userPool.saveUser({
        ...user,
        UserLastModifiedDate: new Date().getTime(),
        UserStatus: "CONFIRMED",
        Enabled: false,
        ConfirmationCode: undefined,
    });

    return {};
};

export const AdminEnableUser = ({
         cognitoClient,
     }: Services): ConfirmForgotPasswordTarget => async (body) => {
    const userPool = await cognitoClient.getUserPool(body.UserPoolId);
    const user = await userPool.getUserByUsername(body.Username);

    if (!user) {
        throw new UserNotFoundError();
    }

    await userPool.saveUser({
        ...user,
        UserLastModifiedDate: new Date().getTime(),
        UserStatus: "CONFIRMED",
        Enabled: true,
        ConfirmationCode: undefined,
    });


    return {};
};
