import {
  InvalidPasswordError,
  NotAuthorizedError,
  PasswordResetRequiredError,
  UnsupportedError,
  DisabledUserError,
} from "../errors";
import { CodeDelivery, Services, UserPoolClient } from "../services";
import { generateTokens, getUserNameFromToken } from "../services/tokens";
import { attributeValue, User } from "../services/userPoolClient";

interface UserPassWord {
  USERNAME: string;
  PASSWORD: string
}
interface RefreshToken { REFRESH_TOKEN: string }
interface Input {
  AuthFlow: "USER_PASSWORD_AUTH" | "CUSTOM_AUTH" | "REFRESH_TOKEN_AUTH";
  ClientId: string;
  AuthParameters: UserPassWord | RefreshToken;
  Session: string | null;
}

export interface SmsMfaOutput {
  ChallengeName: "SMS_MFA";
  ChallengeParameters: {
    CODE_DELIVERY_DELIVERY_MEDIUM: "SMS";
    CODE_DELIVERY_DESTINATION: string;
    USER_ID_FOR_SRP: string;
  };
  Session: string | null;
}

export interface PasswordVerifierOutput {
  ChallengeName: "PASSWORD_VERIFIER" | "REFRESH_TOKEN_VERIFIER";
  ChallengeParameters: {};
  Session: string | null;
  AuthenticationResult: {
    IdToken: string;
    AccessToken: string;
    RefreshToken: string;
  };
}

export type Output = SmsMfaOutput | PasswordVerifierOutput;

export type InitiateAuthTarget = (body: Input) => Promise<Output>;

const verifyMfaChallenge = async (
  user: User,
  body: Input,
  userPool: UserPoolClient,
  codeDelivery: CodeDelivery
): Promise<SmsMfaOutput> => {
  if (!user.MFAOptions?.length) {
    throw new NotAuthorizedError();
  }
  const smsMfaOption = user.MFAOptions?.find((x) => x.DeliveryMedium === "SMS");
  if (!smsMfaOption) {
    throw new UnsupportedError("MFA challenge without SMS");
  }

  const deliveryDestination = attributeValue(
    smsMfaOption.AttributeName,
    user.Attributes
  );
  if (!deliveryDestination) {
    throw new UnsupportedError(`SMS_MFA without ${smsMfaOption.AttributeName}`);
  }

  const code = await codeDelivery(user, {
    ...smsMfaOption,
    Destination: deliveryDestination,
  });

  await userPool.saveUser({
    ...user,
    MFACode: code,
  });

  return {
    ChallengeName: "SMS_MFA",
    ChallengeParameters: {
      CODE_DELIVERY_DELIVERY_MEDIUM: "SMS",
      CODE_DELIVERY_DESTINATION: deliveryDestination,
      USER_ID_FOR_SRP: user.Username,
    },
    Session: body.Session,
  };
};

const verifyPasswordChallenge = (
  user: User,
  body: Input,
  userPool: UserPoolClient
): PasswordVerifierOutput => ({
  ChallengeName: "PASSWORD_VERIFIER",
  ChallengeParameters: {},
  AuthenticationResult: generateTokens(user, body.ClientId, userPool.config.Id),
  Session: body.Session,
});

const verifyRefreshChallenge = (
    user: User,
    body: Input,
    userPool: UserPoolClient
): PasswordVerifierOutput => ({
  ChallengeName: "REFRESH_TOKEN_VERIFIER",
  ChallengeParameters: {},
  AuthenticationResult: generateTokens(user, body.ClientId, userPool.config.Id),
  Session: body.Session,
});

export const InitiateUserPassWordAuth = ({
   codeDelivery,
   cognitoClient,
   triggers,
 }: Services): InitiateAuthTarget => async (body) => {
  const AuthParameters = body.AuthParameters as UserPassWord;
  const userPool = await cognitoClient.getUserPoolForClientId(body.ClientId);
  let user = await userPool.getUserByUsername(AuthParameters.USERNAME);

  if (!user && triggers.enabled("UserMigration")) {
    // https://docs.aws.amazon.com/cognito/latest/developerguide/user-pool-lambda-migrate-user.html
    //
    // Amazon Cognito invokes [the User Migration] trigger when a user does not exist in the user pool at the time of
    // sign-in with a password, or in the forgot-password flow. After the Lambda function returns successfully, Amazon
    // Cognito creates the user in the user pool.
    user = await triggers.userMigration({
      userPoolId: userPool.config.Id,
      clientId: body.ClientId,
      username: AuthParameters.USERNAME,
      password: AuthParameters.PASSWORD,
      userAttributes: [],
    });
  }

  if (!user) {
    throw new NotAuthorizedError();
  }
  if (user.UserStatus === "RESET_REQUIRED") {
    throw new PasswordResetRequiredError();
  }
  if (user.Password !== AuthParameters.PASSWORD) {
    throw new InvalidPasswordError();
  }

  if (!user.Enabled){
    throw new DisabledUserError();
  }

  if (
      (userPool.config.MfaConfiguration === "OPTIONAL" &&
          (user.MFAOptions ?? []).length > 0) ||
      userPool.config.MfaConfiguration === "ON"
  ) {
    return verifyMfaChallenge(user, body, userPool, codeDelivery);
  }

  return verifyPasswordChallenge(user, body, userPool);

};

export const InitiateRefreshTokenAuth = ({
     cognitoClient,
   }: Pick<Services, "cognitoClient">): InitiateAuthTarget => async (body) => {
  const AuthParameters = body.AuthParameters as RefreshToken;
  const username = getUserNameFromToken(AuthParameters.REFRESH_TOKEN)
  const userPool = await cognitoClient.getUserPoolForClientId(body.ClientId);


  let user = await userPool.getUserByUsername(username || '');

  if (!user) {
    throw new NotAuthorizedError();
  }
  if (user.UserStatus === "RESET_REQUIRED") {
    throw new PasswordResetRequiredError();
  }

  if (!user.Enabled){
    throw new DisabledUserError();
  }

  const response =  verifyRefreshChallenge(user, body, userPool);
  delete response.AuthenticationResult.RefreshToken;

  return response;
};

export const InitiateAuth = ({
  codeDelivery,
  cognitoClient,
  triggers,
}: Services): InitiateAuthTarget => async (body) => {
  if (body.AuthFlow === "USER_PASSWORD_AUTH") {
    return InitiateUserPassWordAuth(  {
      codeDelivery,
        cognitoClient,
        triggers})(body);
  }

  if (body.AuthFlow === "REFRESH_TOKEN_AUTH") {
    return InitiateRefreshTokenAuth(  { cognitoClient })(body);
  }

  throw new UnsupportedError(`AuthFlow=${body.AuthFlow}`);
};
