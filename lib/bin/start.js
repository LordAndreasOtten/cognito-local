#!/usr/bin/env node
"use strict";

var _log = _interopRequireDefault(require("../log"));

var _server = require("../server");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

(0, _server.createDefaultServer)().then(server => {
  var _process$env$HOST, _process$env$PORT;

  const hostname = (_process$env$HOST = process.env.HOST) !== null && _process$env$HOST !== void 0 ? _process$env$HOST : "localhost";
  const port = parseInt((_process$env$PORT = process.env.PORT) !== null && _process$env$PORT !== void 0 ? _process$env$PORT : "9229", 10);
  return server.start({
    hostname,
    port
  });
}).then(httpServer => {
  const address = httpServer.address();

  if (!address) {
    throw new Error("Server started without address");
  }

  const url = typeof address === "string" ? address : `${address.address}:${address.port}`;

  _log.default.info(`Cognito Local running on http://${url}`);
}).catch(err => {
  console.error(err);
  process.exit(1);
});