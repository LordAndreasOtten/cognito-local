"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
Object.defineProperty(exports, "createServer", {
  enumerable: true,
  get: function () {
    return _server.createServer;
  }
});
Object.defineProperty(exports, "createDefaultServer", {
  enumerable: true,
  get: function () {
    return _defaults.createDefaultServer;
  }
});

var _server = require("./server");

var _defaults = require("./defaults");