"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.createDefaultServer = void 0;

var _log = _interopRequireDefault(require("../log"));

var _services = require("../services");

var _consoleCodeSender = require("../services/codeDelivery/consoleCodeSender");

var _otp = require("../services/codeDelivery/otp");

var _dataStore = require("../services/dataStore");

var _lambda = require("../services/lambda");

var _triggers = require("../services/triggers");

var _cognitoClient = require("../services/cognitoClient");

var _userPoolClient = require("../services/userPoolClient");

var _router = require("../targets/router");

var _config = require("./config");

var _server = require("./server");

var AWS = _interopRequireWildcard(require("aws-sdk"));

function _getRequireWildcardCache() { if (typeof WeakMap !== "function") return null; var cache = new WeakMap(); _getRequireWildcardCache = function () { return cache; }; return cache; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } if (obj === null || typeof obj !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const createDefaultServer = async () => {
  const config = await (0, _config.loadConfig)();

  _log.default.debug("Loaded config:", config);

  const cognitoClient = await (0, _cognitoClient.createCognitoClient)(config.UserPoolDefaults, _dataStore.createDataStore, _userPoolClient.createUserPoolClient);
  const lambdaClient = new AWS.Lambda(config.LambdaClient);
  const lambda = (0, _lambda.createLambda)(config.TriggerFunctions, lambdaClient);
  const triggers = (0, _triggers.createTriggers)({
    lambda,
    cognitoClient
  });
  const router = (0, _router.Router)({
    codeDelivery: (0, _services.createCodeDelivery)(_consoleCodeSender.ConsoleCodeSender, _otp.otp),
    cognitoClient,
    triggers
  });
  return (0, _server.createServer)(router, {
    development: !!process.env.COGNITO_LOCAL_DEVMODE
  });
};

exports.createDefaultServer = createDefaultServer;