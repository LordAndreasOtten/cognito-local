"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.loadConfig = void 0;

var _deepmerge = _interopRequireDefault(require("deepmerge"));

var _dataStore = require("../services/dataStore");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const defaults = {
  LambdaClient: {
    credentials: {
      accessKeyId: "local",
      secretAccessKey: "local"
    },
    region: "local"
  },
  TriggerFunctions: {},
  UserPoolDefaults: {
    Id: "local",
    UsernameAttributes: ["email"]
  }
};

const loadConfig = async () => {
  const dataStore = await (0, _dataStore.createDataStore)("config", defaults, ".cognito");
  const config = await dataStore.getRoot();
  return (0, _deepmerge.default)(defaults, config !== null && config !== void 0 ? config : {});
};

exports.loadConfig = loadConfig;