"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.createServer = void 0;

var _bodyParser = _interopRequireDefault(require("body-parser"));

var _cors = _interopRequireDefault(require("cors"));

var _express = _interopRequireDefault(require("express"));

var _errors = require("../errors");

var _log = _interopRequireDefault(require("../log"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const PublicKey = {
  jwk: {
    kty: "RSA",
    e: "AQAB",
    use: "sig",
    kid: "CognitoLocal",
    alg: "RS256",
    n: "2uLO7yh1_6Icfd89V3nNTc_qhfpDN7vEmOYlmJQlc9_RmOns26lg88fXXFntZESwHOm7_homO2Ih6NOtu4P5eskGs8d8VQMOQfF4YrP-pawVz-gh1S7eSvzZRDHBT4ItUuoiVP1B9HN_uScKxIqjmitpPqEQB_o2NJv8npCfqUAU-4KmxquGtjdmfctswSZGdz59M3CAYKDfuvLH9_vV6TRGgbUaUAXWC2WJrbbEXzK3XUDBrmF3Xo-yw8f3SgD3JOPl3HaaWMKL1zGVAsge7gQaGiJBzBurg5vwN61uDGGz0QZC1JqcUTl3cZnrx_L8isIR7074SJEuljIZRnCcjQ"
  },
  pem: "-----BEGIN RSA PUBLIC KEY-----\nMIIBCgKCAQEA2uLO7yh1/6Icfd89V3nNTc/qhfpDN7vEmOYlmJQlc9/RmOns26lg\n88fXXFntZESwHOm7/homO2Ih6NOtu4P5eskGs8d8VQMOQfF4YrP+pawVz+gh1S7e\nSvzZRDHBT4ItUuoiVP1B9HN/uScKxIqjmitpPqEQB/o2NJv8npCfqUAU+4KmxquG\ntjdmfctswSZGdz59M3CAYKDfuvLH9/vV6TRGgbUaUAXWC2WJrbbEXzK3XUDBrmF3\nXo+yw8f3SgD3JOPl3HaaWMKL1zGVAsge7gQaGiJBzBurg5vwN61uDGGz0QZC1Jqc\nUTl3cZnrx/L8isIR7074SJEuljIZRnCcjQIDAQAB\n-----END RSA PUBLIC KEY-----"
};

const createServer = (router, options = {}) => {
  const app = (0, _express.default)();
  app.use((0, _cors.default)({
    origin: "*"
  }));
  app.use(_bodyParser.default.json({
    type: "application/x-amz-json-1.1"
  }));
  app.get("/:userPoolId/.well-known/jwks.json", (req, res) => {
    res.status(200).json({
      keys: [PublicKey.jwk]
    });
  });
  app.post("/", async (req, res) => {
    const xAmzTarget = req.headers["x-amz-target"];

    if (!xAmzTarget) {
      return res.status(400).json({
        message: "Missing x-amz-target header"
      });
    } else if (xAmzTarget instanceof Array) {
      return res.status(400).json({
        message: "Too many x-amz-target headers"
      });
    }

    const [, target] = xAmzTarget.split(".");

    if (!target) {
      return res.status(400).json({
        message: "Invalid x-amz-target header"
      });
    }

    const route = router(target);

    try {
      const output = await route(req.body);
      return res.status(200).json(output);
    } catch (ex) {
      console.error(`Error handling target: ${target}`, ex);

      if (ex instanceof _errors.UnsupportedError) {
        if (options.development) {
          _log.default.info("======");

          _log.default.info();

          _log.default.info("Unsupported target");

          _log.default.info("");

          _log.default.info(`x-amz-target: ${xAmzTarget}`);

          _log.default.info("Body:");

          _log.default.info(JSON.stringify(req.body, undefined, 2));

          _log.default.info();

          _log.default.info("======");
        }

        return (0, _errors.unsupported)(ex.message, res);
      } else if (ex instanceof _errors.CognitoError) {
        return res.status(400).json({
          code: ex.code,
          message: ex.message
        });
      } else {
        return res.status(500).json(ex);
      }
    }
  });
  return {
    application: app,

    start(startOptions) {
      var _options$port, _options$hostname, _options$development;

      const actualOptions = {
        port: (_options$port = options === null || options === void 0 ? void 0 : options.port) !== null && _options$port !== void 0 ? _options$port : 9229,
        hostname: (_options$hostname = options === null || options === void 0 ? void 0 : options.hostname) !== null && _options$hostname !== void 0 ? _options$hostname : "localhost",
        development: (_options$development = options === null || options === void 0 ? void 0 : options.development) !== null && _options$development !== void 0 ? _options$development : false,
        ...options,
        ...startOptions
      };
      return new Promise((resolve, reject) => {
        try {
          const httpServer = app.listen(actualOptions.port, actualOptions.hostname, () => resolve(httpServer));
        } catch (err) {
          reject(err);
        }
      });
    }

  };
};

exports.createServer = createServer;