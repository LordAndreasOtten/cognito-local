"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.ForgotPassword = void 0;

var _errors = require("../errors");

const ForgotPassword = ({
  cognitoClient,
  codeDelivery
}) => async body => {
  const userPool = await cognitoClient.getUserPoolForClientId(body.ClientId);
  const user = await userPool.getUserByUsername(body.Username);

  if (!user) {
    throw new _errors.UserNotFoundError();
  }

  const deliveryDetails = {
    AttributeName: "email",
    DeliveryMedium: "EMAIL",
    Destination: user.Attributes.filter(x => x.Name === "email").map(x => x.Value)[0]
  };
  const code = await codeDelivery(user, deliveryDetails);
  await userPool.saveUser({ ...user,
    UserLastModifiedDate: new Date().getTime(),
    ConfirmationCode: code
  });
  return {
    CodeDeliveryDetails: deliveryDetails
  };
};

exports.ForgotPassword = ForgotPassword;