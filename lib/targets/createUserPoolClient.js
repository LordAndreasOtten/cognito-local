"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.CreateUserPoolClient = void 0;

const CreateUserPoolClient = ({
  cognitoClient
}) => async body => {
  const userPool = await cognitoClient.getUserPool(body.UserPoolId);
  const appClient = await userPool.createAppClient(body.ClientName);
  return {
    UserPoolClient: appClient
  };
};

exports.CreateUserPoolClient = CreateUserPoolClient;