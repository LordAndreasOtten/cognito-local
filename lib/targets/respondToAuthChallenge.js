"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.RespondToAuthChallenge = void 0;

var _errors = require("../errors");

var _tokens = require("../services/tokens");

const RespondToAuthChallenge = ({
  cognitoClient
}) => async body => {
  const userPool = await cognitoClient.getUserPoolForClientId(body.ClientId);
  const user = await userPool.getUserByUsername(body.ChallengeResponses.USERNAME);

  if (!user) {
    throw new _errors.NotAuthorizedError();
  }

  if (user.MFACode !== body.ChallengeResponses.SMS_MFA_CODE) {
    throw new _errors.CodeMismatchError();
  }

  await userPool.saveUser({ ...user,
    MFACode: undefined
  });
  return {
    ChallengeName: body.ChallengeName,
    ChallengeParameters: {},
    AuthenticationResult: (0, _tokens.generateTokens)(user, body.ClientId, userPool.config.Id),
    Session: body.Session
  };
};

exports.RespondToAuthChallenge = RespondToAuthChallenge;