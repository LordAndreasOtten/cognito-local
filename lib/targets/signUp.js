"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.SignUp = void 0;

var uuid = _interopRequireWildcard(require("uuid"));

var _errors = require("../errors");

function _getRequireWildcardCache() { if (typeof WeakMap !== "function") return null; var cache = new WeakMap(); _getRequireWildcardCache = function () { return cache; }; return cache; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } if (obj === null || typeof obj !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

const SignUp = ({
  cognitoClient,
  codeDelivery
}) => async body => {
  // TODO: This should behave differently depending on if PreventUserExistenceErrors
  // is enabled on the user pool. This will be the default after Feb 2020.
  // See: https://docs.aws.amazon.com/cognito/latest/developerguide/cognito-user-pool-managing-errors.html
  const userPool = await cognitoClient.getUserPoolForClientId(body.ClientId);
  const existingUser = await userPool.getUserByUsername(body.Username);

  if (existingUser) {
    throw new _errors.UsernameExistsError();
  }

  const user = {
    Attributes: body.UserAttributes,
    Enabled: true,
    Password: body.Password,
    UserCreateDate: new Date().getTime(),
    UserLastModifiedDate: new Date().getTime(),
    UserStatus: "UNCONFIRMED",
    Username: body.Username || uuid.v4()
  };
  const deliveryDetails = {
    AttributeName: "email",
    DeliveryMedium: "EMAIL",
    Destination: user.Attributes.filter(x => x.Name === "email").map(x => x.Value)[0]
  };
  const code = await codeDelivery(user, deliveryDetails);
  await userPool.saveUser({ ...user,
    ConfirmationCode: code
  });
  return {
    UserConfirmed: user.UserStatus === "CONFIRMED",
    UserSub: user.Username,
    CodeDeliveryDetails: deliveryDetails
  };
};

exports.SignUp = SignUp;