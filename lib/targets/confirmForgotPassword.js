"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.ConfirmForgotPassword = void 0;

var _errors = require("../errors");

const ConfirmForgotPassword = ({
  cognitoClient,
  triggers
}) => async body => {
  const userPool = await cognitoClient.getUserPoolForClientId(body.ClientId);
  const user = await userPool.getUserByUsername(body.Username);

  if (!user) {
    throw new _errors.UserNotFoundError();
  }

  if (user.ConfirmationCode !== body.ConfirmationCode) {
    throw new _errors.CodeMismatchError();
  }

  await userPool.saveUser({ ...user,
    UserLastModifiedDate: new Date().getTime(),
    UserStatus: "CONFIRMED",
    ConfirmationCode: undefined,
    Password: body.Password
  });

  if (triggers.enabled("PostConfirmation")) {
    await triggers.postConfirmation({
      source: "PostConfirmation_ConfirmForgotPassword",
      username: user.Username,
      clientId: body.ClientId,
      userPoolId: userPool.config.Id,
      userAttributes: user.Attributes
    });
  }

  return {};
};

exports.ConfirmForgotPassword = ConfirmForgotPassword;