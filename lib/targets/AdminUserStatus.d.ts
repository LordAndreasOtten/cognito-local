import { Services } from "../services";
interface Input {
    UserPoolId: string;
    Username: string;
}
export declare type ConfirmForgotPasswordTarget = (body: Input) => Promise<{}>;
export declare const AdminDisableUser: ({ cognitoClient, }: Services) => ConfirmForgotPasswordTarget;
export declare const AdminEnableUser: ({ cognitoClient, }: Services) => ConfirmForgotPasswordTarget;
export {};
