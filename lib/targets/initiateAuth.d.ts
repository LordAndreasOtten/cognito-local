import { Services } from "../services";
interface UserPassWord {
    USERNAME: string;
    PASSWORD: string;
}
interface RefreshToken {
    REFRESH_TOKEN: string;
}
interface Input {
    AuthFlow: "USER_PASSWORD_AUTH" | "CUSTOM_AUTH" | "REFRESH_TOKEN_AUTH";
    ClientId: string;
    AuthParameters: UserPassWord | RefreshToken;
    Session: string | null;
}
export interface SmsMfaOutput {
    ChallengeName: "SMS_MFA";
    ChallengeParameters: {
        CODE_DELIVERY_DELIVERY_MEDIUM: "SMS";
        CODE_DELIVERY_DESTINATION: string;
        USER_ID_FOR_SRP: string;
    };
    Session: string | null;
}
export interface PasswordVerifierOutput {
    ChallengeName: "PASSWORD_VERIFIER" | "REFRESH_TOKEN_VERIFIER";
    ChallengeParameters: {};
    Session: string | null;
    AuthenticationResult: {
        IdToken: string;
        AccessToken: string;
        RefreshToken: string;
    };
}
export declare type Output = SmsMfaOutput | PasswordVerifierOutput;
export declare type InitiateAuthTarget = (body: Input) => Promise<Output>;
export declare const InitiateUserPassWordAuth: ({ codeDelivery, cognitoClient, triggers, }: Services) => InitiateAuthTarget;
export declare const InitiateRefreshTokenAuth: ({ cognitoClient, }: Pick<Services, "cognitoClient">) => InitiateAuthTarget;
export declare const InitiateAuth: ({ codeDelivery, cognitoClient, triggers, }: Services) => InitiateAuthTarget;
export {};
