"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.ListUsers = void 0;

const ListUsers = ({
  cognitoClient
}) => async body => {
  const userPool = await cognitoClient.getUserPool(body.UserPoolId);
  const users = await userPool.listUsers();
  return {
    Users: users.map(user => ({
      Username: user.Username,
      UserCreateDate: user.UserCreateDate,
      UserLastModifiedDate: user.UserLastModifiedDate,
      Enabled: user.Enabled,
      UserStatus: user.UserStatus,
      Attributes: user.Attributes
    }))
  };
};

exports.ListUsers = ListUsers;