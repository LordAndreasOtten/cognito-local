"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.GetUser = void 0;

var _jsonwebtoken = _interopRequireDefault(require("jsonwebtoken"));

var _errors = require("../errors");

var _log = _interopRequireDefault(require("../log"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const GetUser = ({
  cognitoClient
}) => async (body) => {
  const decodedToken = _jsonwebtoken.default.decode(body.AccessToken);

  if (!decodedToken) {
    _log.default.info("Unable to decode token");

    throw new _errors.InvalidParameterError();
  }

  const {
    sub,
    client_id
  } = decodedToken;

  if (!sub || !client_id) {
    return null;
  }

  const userPool = await cognitoClient.getUserPoolForClientId(client_id);
  const user = await userPool.getUserByUsername(sub);

  if (!user) {
    return null;
  }

  const output = {
    Username: user.Username,
    UserAttributes: user.Attributes
  };

  if (user.MFAOptions) {
    output.MFAOptions = user.MFAOptions;
  }

  return output;
};

exports.GetUser = GetUser;