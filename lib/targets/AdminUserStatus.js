"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.AdminEnableUser = exports.AdminDisableUser = void 0;

var _errors = require("../errors");

const AdminDisableUser = ({
  cognitoClient
}) => async body => {
  const userPool = await cognitoClient.getUserPool(body.UserPoolId);
  const user = await userPool.getUserByUsername(body.Username);

  if (!user) {
    throw new _errors.UserNotFoundError();
  }

  await userPool.saveUser({ ...user,
    UserLastModifiedDate: new Date().getTime(),
    UserStatus: "CONFIRMED",
    Enabled: false,
    ConfirmationCode: undefined
  });
  return {};
};

exports.AdminDisableUser = AdminDisableUser;

const AdminEnableUser = ({
  cognitoClient
}) => async body => {
  const userPool = await cognitoClient.getUserPool(body.UserPoolId);
  const user = await userPool.getUserByUsername(body.Username);

  if (!user) {
    throw new _errors.UserNotFoundError();
  }

  await userPool.saveUser({ ...user,
    UserLastModifiedDate: new Date().getTime(),
    UserStatus: "CONFIRMED",
    Enabled: true,
    ConfirmationCode: undefined
  });
  return {};
};

exports.AdminEnableUser = AdminEnableUser;