"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.ConfirmSignUp = void 0;

var _errors = require("../errors");

const ConfirmSignUp = ({
  cognitoClient,
  triggers
}) => async body => {
  const userPool = await cognitoClient.getUserPoolForClientId(body.ClientId);
  const user = await userPool.getUserByUsername(body.Username);

  if (!user) {
    throw new _errors.NotAuthorizedError();
  }

  if (user.ConfirmationCode !== body.ConfirmationCode) {
    throw new _errors.CodeMismatchError();
  }

  await userPool.saveUser({ ...user,
    UserStatus: "CONFIRMED",
    ConfirmationCode: undefined,
    UserLastModifiedDate: new Date().getTime()
  });

  if (triggers.enabled("PostConfirmation")) {
    await triggers.postConfirmation({
      source: "PostConfirmation_ConfirmSignUp",
      username: user.Username,
      clientId: body.ClientId,
      userPoolId: userPool.config.Id,
      userAttributes: user.Attributes
    });
  }
};

exports.ConfirmSignUp = ConfirmSignUp;