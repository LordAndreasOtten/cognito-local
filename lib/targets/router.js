"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Router = exports.isSupportedTarget = exports.Targets = void 0;

var _errors = require("../errors");

var _confirmForgotPassword = require("./confirmForgotPassword");

var _confirmSignUp = require("./confirmSignUp");

var _createUserPoolClient = require("./createUserPoolClient");

var _forgotPassword = require("./forgotPassword");

var _initiateAuth = require("./initiateAuth");

var _listUsers = require("./listUsers");

var _respondToAuthChallenge = require("./respondToAuthChallenge");

var _signUp = require("./signUp");

var _getUser = require("./getUser");

var _AdminUserStatus = require("./AdminUserStatus");

const Targets = {
  ConfirmForgotPassword: _confirmForgotPassword.ConfirmForgotPassword,
  ConfirmSignUp: _confirmSignUp.ConfirmSignUp,
  CreateUserPoolClient: _createUserPoolClient.CreateUserPoolClient,
  ForgotPassword: _forgotPassword.ForgotPassword,
  InitiateAuth: _initiateAuth.InitiateAuth,
  ListUsers: _listUsers.ListUsers,
  RespondToAuthChallenge: _respondToAuthChallenge.RespondToAuthChallenge,
  SignUp: _signUp.SignUp,
  GetUser: _getUser.GetUser,
  AdminDisableUser: _AdminUserStatus.AdminDisableUser,
  AdminEnableUser: _AdminUserStatus.AdminEnableUser
};
exports.Targets = Targets;

const isSupportedTarget = name => Object.keys(Targets).includes(name); // eslint-disable-next-line


exports.isSupportedTarget = isSupportedTarget;

const Router = services => target => {
  if (!isSupportedTarget(target)) {
    return () => Promise.reject(new _errors.UnsupportedError(`Unsupported x-amz-target header "${target}"`));
  }

  return Targets[target](services);
};

exports.Router = Router;