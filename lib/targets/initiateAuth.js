"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.InitiateAuth = exports.InitiateRefreshTokenAuth = exports.InitiateUserPassWordAuth = void 0;

var _errors = require("../errors");

var _tokens = require("../services/tokens");

var _userPoolClient = require("../services/userPoolClient");

const verifyMfaChallenge = async (user, body, userPool, codeDelivery) => {
  var _user$MFAOptions, _user$MFAOptions2;

  if (!((_user$MFAOptions = user.MFAOptions) !== null && _user$MFAOptions !== void 0 && _user$MFAOptions.length)) {
    throw new _errors.NotAuthorizedError();
  }

  const smsMfaOption = (_user$MFAOptions2 = user.MFAOptions) === null || _user$MFAOptions2 === void 0 ? void 0 : _user$MFAOptions2.find(x => x.DeliveryMedium === "SMS");

  if (!smsMfaOption) {
    throw new _errors.UnsupportedError("MFA challenge without SMS");
  }

  const deliveryDestination = (0, _userPoolClient.attributeValue)(smsMfaOption.AttributeName, user.Attributes);

  if (!deliveryDestination) {
    throw new _errors.UnsupportedError(`SMS_MFA without ${smsMfaOption.AttributeName}`);
  }

  const code = await codeDelivery(user, { ...smsMfaOption,
    Destination: deliveryDestination
  });
  await userPool.saveUser({ ...user,
    MFACode: code
  });
  return {
    ChallengeName: "SMS_MFA",
    ChallengeParameters: {
      CODE_DELIVERY_DELIVERY_MEDIUM: "SMS",
      CODE_DELIVERY_DESTINATION: deliveryDestination,
      USER_ID_FOR_SRP: user.Username
    },
    Session: body.Session
  };
};

const verifyPasswordChallenge = (user, body, userPool) => ({
  ChallengeName: "PASSWORD_VERIFIER",
  ChallengeParameters: {},
  AuthenticationResult: (0, _tokens.generateTokens)(user, body.ClientId, userPool.config.Id),
  Session: body.Session
});

const verifyRefreshChallenge = (user, body, userPool) => ({
  ChallengeName: "REFRESH_TOKEN_VERIFIER",
  ChallengeParameters: {},
  AuthenticationResult: (0, _tokens.generateTokens)(user, body.ClientId, userPool.config.Id),
  Session: body.Session
});

const InitiateUserPassWordAuth = ({
  codeDelivery,
  cognitoClient,
  triggers
}) => async body => {
  var _user$MFAOptions3;

  const AuthParameters = body.AuthParameters;
  const userPool = await cognitoClient.getUserPoolForClientId(body.ClientId);
  let user = await userPool.getUserByUsername(AuthParameters.USERNAME);

  if (!user && triggers.enabled("UserMigration")) {
    // https://docs.aws.amazon.com/cognito/latest/developerguide/user-pool-lambda-migrate-user.html
    //
    // Amazon Cognito invokes [the User Migration] trigger when a user does not exist in the user pool at the time of
    // sign-in with a password, or in the forgot-password flow. After the Lambda function returns successfully, Amazon
    // Cognito creates the user in the user pool.
    user = await triggers.userMigration({
      userPoolId: userPool.config.Id,
      clientId: body.ClientId,
      username: AuthParameters.USERNAME,
      password: AuthParameters.PASSWORD,
      userAttributes: []
    });
  }

  if (!user) {
    throw new _errors.NotAuthorizedError();
  }

  if (user.UserStatus === "RESET_REQUIRED") {
    throw new _errors.PasswordResetRequiredError();
  }

  if (user.Password !== AuthParameters.PASSWORD) {
    throw new _errors.InvalidPasswordError();
  }

  if (!user.Enabled) {
    throw new _errors.DisabledUserError();
  }

  if (userPool.config.MfaConfiguration === "OPTIONAL" && ((_user$MFAOptions3 = user.MFAOptions) !== null && _user$MFAOptions3 !== void 0 ? _user$MFAOptions3 : []).length > 0 || userPool.config.MfaConfiguration === "ON") {
    return verifyMfaChallenge(user, body, userPool, codeDelivery);
  }

  return verifyPasswordChallenge(user, body, userPool);
};

exports.InitiateUserPassWordAuth = InitiateUserPassWordAuth;

const InitiateRefreshTokenAuth = ({
  cognitoClient
}) => async body => {
  const AuthParameters = body.AuthParameters;
  const username = (0, _tokens.getUserNameFromToken)(AuthParameters.REFRESH_TOKEN);
  const userPool = await cognitoClient.getUserPoolForClientId(body.ClientId);
  let user = await userPool.getUserByUsername(username || '');

  if (!user) {
    throw new _errors.NotAuthorizedError();
  }

  if (user.UserStatus === "RESET_REQUIRED") {
    throw new _errors.PasswordResetRequiredError();
  }

  if (!user.Enabled) {
    throw new _errors.DisabledUserError();
  }

  const response = verifyRefreshChallenge(user, body, userPool);
  delete response.AuthenticationResult.RefreshToken;
  return response;
};

exports.InitiateRefreshTokenAuth = InitiateRefreshTokenAuth;

const InitiateAuth = ({
  codeDelivery,
  cognitoClient,
  triggers
}) => async body => {
  if (body.AuthFlow === "USER_PASSWORD_AUTH") {
    return InitiateUserPassWordAuth({
      codeDelivery,
      cognitoClient,
      triggers
    })(body);
  }

  if (body.AuthFlow === "REFRESH_TOKEN_AUTH") {
    return InitiateRefreshTokenAuth({
      cognitoClient
    })(body);
  }

  throw new _errors.UnsupportedError(`AuthFlow=${body.AuthFlow}`);
};

exports.InitiateAuth = InitiateAuth;