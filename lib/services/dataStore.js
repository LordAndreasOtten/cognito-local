"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.createDataStore = void 0;

var _fs = _interopRequireDefault(require("fs"));

var _stormdb = _interopRequireDefault(require("stormdb"));

var _util = require("util");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const mkdir = (0, _util.promisify)(_fs.default.mkdir);

const createDataStore = async (id, defaults, directory = ".cognito/db") => {
  await mkdir(directory, {
    recursive: true
  });
  const engine = new _stormdb.default.localFileEngine(`${directory}/${id}.json`, {
    async: true,
    serialize: obj => JSON.stringify(obj, undefined, 2)
  });
  const db = new _stormdb.default(engine);
  db.default(defaults);
  return {
    async getRoot() {
      var _await$db$value;

      return (_await$db$value = await db.value()) !== null && _await$db$value !== void 0 ? _await$db$value : null;
    },

    async get(key, defaultValue) {
      var _ref, _await$reduce$value;

      return (_ref = (_await$reduce$value = await (key instanceof Array ? key : [key]).reduce((acc, k) => acc.get(k), db).value()) !== null && _await$reduce$value !== void 0 ? _await$reduce$value : defaultValue) !== null && _ref !== void 0 ? _ref : null;
    },

    async set(key, value) {
      if (typeof key === 'string' && key.includes("@")) {
        var n = key.lastIndexOf(".");
        key = key.slice(0, n) + key.slice(n).replace(".", "");
      }

      await db.set(key, value).save();
    }

  };
};

exports.createDataStore = createDataStore;