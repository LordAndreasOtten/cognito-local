"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.createUserPoolClient = exports.attributesFromRecord = exports.attributesToRecord = exports.attributeValue = exports.attributesInclude = exports.attributesIncludeMatch = void 0;

var _log = _interopRequireDefault(require("../log"));

var _appClient = require("./appClient");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const attributesIncludeMatch = (attributeName, attributeValue, attributes) => !!attributes.find(x => x.Name === attributeName && x.Value === attributeValue);

exports.attributesIncludeMatch = attributesIncludeMatch;

const attributesInclude = (attributeName, attributes) => !!attributes.find(x => x.Name === attributeName);

exports.attributesInclude = attributesInclude;

const attributeValue = (attributeName, attributes) => {
  var _attributes$find;

  return (_attributes$find = attributes.find(x => x.Name === attributeName)) === null || _attributes$find === void 0 ? void 0 : _attributes$find.Value;
};

exports.attributeValue = attributeValue;

const attributesToRecord = attributes => attributes.reduce((acc, attr) => ({ ...acc,
  [attr.Name]: attr.Value
}), {});

exports.attributesToRecord = attributesToRecord;

const attributesFromRecord = attributes => Object.entries(attributes).map(([Name, Value]) => ({
  Name,
  Value
}));

exports.attributesFromRecord = attributesFromRecord;

const createUserPoolClient = async (defaultOptions, clientsDataStore, createDataStore) => {
  const dataStore = await createDataStore(defaultOptions.Id, {
    Users: {},
    Options: defaultOptions
  });
  const config = await dataStore.get("Options", defaultOptions);
  return {
    config,

    async createAppClient(name) {
      const id = (0, _appClient.newId)();
      const appClient = {
        ClientId: id,
        ClientName: name,
        UserPoolId: defaultOptions.Id,
        CreationDate: new Date().getTime(),
        LastModifiedDate: new Date().getTime(),
        AllowedOAuthFlowsUserPoolClient: false,
        RefreshTokenValidity: 30
      };
      await clientsDataStore.set(["Clients", id], appClient);
      return appClient;
    },

    async getUserByUsername(username) {
      var _config$UsernameAttri, _config$UsernameAttri2;

      _log.default.debug("getUserByUsername", username);

      const aliasEmailEnabled = (_config$UsernameAttri = config.UsernameAttributes) === null || _config$UsernameAttri === void 0 ? void 0 : _config$UsernameAttri.includes("email");
      const aliasPhoneNumberEnabled = (_config$UsernameAttri2 = config.UsernameAttributes) === null || _config$UsernameAttri2 === void 0 ? void 0 : _config$UsernameAttri2.includes("phone_number");
      const users = await dataStore.get("Users", {});

      for (const user of Object.values(users)) {
        if (attributesIncludeMatch("sub", username, user.Attributes)) {
          return user;
        }

        if (aliasEmailEnabled && attributesIncludeMatch("email", username, user.Attributes)) {
          return user;
        }

        if (aliasPhoneNumberEnabled && attributesIncludeMatch("phone_number", username, user.Attributes)) {
          return user;
        }
      }

      return null;
    },

    async listUsers() {
      _log.default.debug("listUsers");

      const users = await dataStore.get("Users", {});
      return Object.values(users);
    },

    async saveUser(user) {
      _log.default.debug("saveUser", user);

      const attributes = attributesInclude("sub", user.Attributes) ? user.Attributes : [{
        Name: "sub",
        Value: user.Username
      }, ...user.Attributes];
      await dataStore.set(`Users.${user.Username}`, { ...user,
        Attributes: attributes
      });
    }

  };
};

exports.createUserPoolClient = createUserPoolClient;