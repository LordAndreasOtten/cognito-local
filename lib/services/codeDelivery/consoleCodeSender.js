"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.ConsoleCodeSender = void 0;

var _boxen = _interopRequireDefault(require("boxen"));

var _log = _interopRequireDefault(require("../../log"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const sendToConsole = (user, destination, code) => {
  _log.default.info((0, _boxen.default)(`Confirmation Code Delivery

Username:    ${user.Username}
Destination: ${destination}
Code:        ${code}`, {
    borderStyle: "round",
    borderColor: "yellow",
    padding: 1
  }));

  return Promise.resolve();
};

const ConsoleCodeSender = {
  sendEmail: sendToConsole,
  sendSms: sendToConsole
};
exports.ConsoleCodeSender = ConsoleCodeSender;