"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.createCodeDelivery = void 0;

const createCodeDelivery = (sender, otp) => async (user, deliveryDetails) => {
  const code = otp();

  if (deliveryDetails.DeliveryMedium === "SMS") {
    await sender.sendSms(user, deliveryDetails.Destination, code);
  } else if (deliveryDetails.DeliveryMedium === "EMAIL") {
    await sender.sendEmail(user, deliveryDetails.Destination, code);
  }

  return Promise.resolve(code);
};

exports.createCodeDelivery = createCodeDelivery;