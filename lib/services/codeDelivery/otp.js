"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.otp = void 0;

const otp = () => Math.floor(Math.random() * 9999).toString().padStart(4, "0");

exports.otp = otp;