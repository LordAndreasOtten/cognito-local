"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.newId = void 0;

var _shortUuid = _interopRequireDefault(require("short-uuid"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const generator = (0, _shortUuid.default)("0123456789abcdefghijklmnopqrstuvwxyz");
const newId = generator.new;
exports.newId = newId;