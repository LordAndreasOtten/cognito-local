"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.createCognitoClient = void 0;

var _errors = require("../errors");

const createCognitoClient = async (userPoolDefaultOptions, createDataStore, createUserPoolClient) => {
  const clients = await createDataStore("clients", {
    Clients: {}
  });
  return {
    async getUserPool(userPoolId) {
      return createUserPoolClient({ ...userPoolDefaultOptions,
        Id: userPoolId
      }, clients, createDataStore);
    },

    async getUserPoolForClientId(clientId) {
      const appClient = await clients.get(["Clients", clientId]);

      if (!appClient) {
        throw new _errors.ResourceNotFoundError();
      }

      return createUserPoolClient({ ...userPoolDefaultOptions,
        Id: appClient.UserPoolId
      }, clients, createDataStore);
    }

  };
};

exports.createCognitoClient = createCognitoClient;