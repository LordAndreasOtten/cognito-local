"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.UserMigration = void 0;

var uuid = _interopRequireWildcard(require("uuid"));

var _errors = require("../../errors");

var _userPoolClient = require("../userPoolClient");

function _getRequireWildcardCache() { if (typeof WeakMap !== "function") return null; var cache = new WeakMap(); _getRequireWildcardCache = function () { return cache; }; return cache; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } if (obj === null || typeof obj !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

const UserMigration = ({
  lambda,
  cognitoClient
}) => async ({
  userPoolId,
  clientId,
  username,
  password,
  userAttributes
}) => {
  var _result$userAttribute, _result$finalUserStat;

  const userPool = await cognitoClient.getUserPoolForClientId(clientId);

  if (!userPool) {
    throw new _errors.ResourceNotFoundError();
  }

  let result;

  try {
    result = await lambda.invoke("UserMigration", {
      userPoolId,
      clientId,
      username,
      password,
      triggerSource: "UserMigration_Authentication",
      userAttributes: (0, _userPoolClient.attributesToRecord)(userAttributes)
    });
  } catch (ex) {
    throw new _errors.NotAuthorizedError();
  }

  const user = {
    Attributes: (0, _userPoolClient.attributesFromRecord)((_result$userAttribute = result.userAttributes) !== null && _result$userAttribute !== void 0 ? _result$userAttribute : {}),
    Enabled: true,
    Password: password,
    UserCreateDate: new Date().getTime(),
    UserLastModifiedDate: new Date().getTime(),
    Username: uuid.v4(),
    UserStatus: (_result$finalUserStat = result.finalUserStatus) !== null && _result$finalUserStat !== void 0 ? _result$finalUserStat : "CONFIRMED"
  };

  if (result.forceAliasCreation) {// TODO: do something with aliases?
  }

  await userPool.saveUser(user);

  if (result.messageAction !== "SUPPRESS") {// TODO: send notification when not suppressed?
  }

  return user;
};

exports.UserMigration = UserMigration;