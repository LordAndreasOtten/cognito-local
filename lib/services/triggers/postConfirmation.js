"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.PostConfirmation = void 0;

var _userPoolClient = require("../userPoolClient");

var _errors = require("../../errors");

const PostConfirmation = ({
  lambda,
  cognitoClient
}) => async ({
  source,
  userPoolId,
  clientId,
  username,
  userAttributes
}) => {
  try {
    const userPool = await cognitoClient.getUserPoolForClientId(clientId);

    if (!userPool) {
      throw new _errors.ResourceNotFoundError();
    }

    await lambda.invoke("PostConfirmation", {
      userPoolId,
      clientId,
      username,
      triggerSource: source,
      userAttributes: (0, _userPoolClient.attributesToRecord)(userAttributes)
    });
  } catch (ex) {
    console.error(ex);
  }
};

exports.PostConfirmation = PostConfirmation;