"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.createTriggers = void 0;

var _postConfirmation = require("./postConfirmation");

var _userMigration = require("./userMigration");

const createTriggers = services => ({
  enabled: trigger => services.lambda.enabled(trigger),
  userMigration: (0, _userMigration.UserMigration)(services),
  postConfirmation: (0, _postConfirmation.PostConfirmation)(services)
});

exports.createTriggers = createTriggers;