"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.createLambda = void 0;

var fs = _interopRequireWildcard(require("fs"));

var _errors = require("../errors");

var _log = _interopRequireDefault(require("../log"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _getRequireWildcardCache() { if (typeof WeakMap !== "function") return null; var cache = new WeakMap(); _getRequireWildcardCache = function () { return cache; }; return cache; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } if (obj === null || typeof obj !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

const awsSdkPackageJson = fs.readFileSync(require.resolve("aws-sdk/package.json"), "utf-8");
const awsSdkVersion = JSON.parse(awsSdkPackageJson).version;

const createLambda = (config, lambdaClient) => ({
  enabled: lambda => !!config[lambda],

  async invoke(trigger, event) {
    const functionName = config[trigger];

    if (!functionName) {
      throw new Error(`${trigger} trigger not configured`);
    }

    const lambdaEvent = {
      version: 0,
      // TODO: how do we know what this is?
      userName: event.username,
      callerContext: {
        awsSdkVersion,
        clientId: event.clientId
      },
      region: "local",
      // TODO: pull from above,
      userPoolId: event.userPoolId,
      triggerSource: event.triggerSource,
      request: {
        userAttributes: event.userAttributes
      },
      response: {}
    };

    if (event.triggerSource === "UserMigration_Authentication") {
      lambdaEvent.request.password = event.password;
      lambdaEvent.request.validationData = {};
    }

    _log.default.debug(`Invoking "${functionName}" with event`, JSON.stringify(lambdaEvent, undefined, 2));

    let result;

    try {
      result = await lambdaClient.invoke({
        FunctionName: functionName,
        InvocationType: "RequestResponse",
        Payload: JSON.stringify(lambdaEvent)
      }).promise();
    } catch (ex) {
      _log.default.error(ex);

      throw new _errors.UnexpectedLambdaExceptionError();
    }

    _log.default.debug(`Lambda completed with StatusCode=${result.StatusCode} and FunctionError=${result.FunctionError}`);

    if (result.StatusCode === 200) {
      const parsedPayload = JSON.parse(result.Payload);
      return parsedPayload.response;
    } else {
      console.error(result.FunctionError);
      throw new _errors.UnexpectedLambdaExceptionError();
    }
  }

});

exports.createLambda = createLambda;