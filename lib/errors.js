"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.unsupported = exports.InvalidParameterError = exports.DisabledUserError = exports.UnexpectedLambdaExceptionError = exports.ResourceNotFoundError = exports.PasswordResetRequiredError = exports.InvalidPasswordError = exports.CodeMismatchError = exports.UsernameExistsError = exports.UserNotFoundError = exports.NotAuthorizedError = exports.CognitoError = exports.UnsupportedError = void 0;

class UnsupportedError extends Error {}

exports.UnsupportedError = UnsupportedError;

class CognitoError extends Error {
  constructor(code, message) {
    super(message);
    this.code = `CognitoLocal#${code}`;
  }

}

exports.CognitoError = CognitoError;

class NotAuthorizedError extends CognitoError {
  constructor() {
    super("NotAuthorizedException", "User not authorized");
  }

}

exports.NotAuthorizedError = NotAuthorizedError;

class UserNotFoundError extends CognitoError {
  constructor() {
    super("UserNotFoundException", "User not found");
  }

}

exports.UserNotFoundError = UserNotFoundError;

class UsernameExistsError extends CognitoError {
  constructor() {
    super("UsernameExistsException", "User already exists");
  }

}

exports.UsernameExistsError = UsernameExistsError;

class CodeMismatchError extends CognitoError {
  constructor() {
    super("CodeMismatchException", "Incorrect confirmation code");
  }

}

exports.CodeMismatchError = CodeMismatchError;

class InvalidPasswordError extends CognitoError {
  constructor() {
    super("InvalidPasswordException", "Invalid password");
  }

}

exports.InvalidPasswordError = InvalidPasswordError;

class PasswordResetRequiredError extends CognitoError {
  constructor() {
    super("PasswordResetRequiredException", "Password reset required");
  }

}

exports.PasswordResetRequiredError = PasswordResetRequiredError;

class ResourceNotFoundError extends CognitoError {
  constructor() {
    super("ResourceNotFoundException", "Resource not found");
  }

}

exports.ResourceNotFoundError = ResourceNotFoundError;

class UnexpectedLambdaExceptionError extends CognitoError {
  constructor() {
    super("UnexpectedLambdaExceptionException", "Unexpected error when invoking lambda");
  }

}

exports.UnexpectedLambdaExceptionError = UnexpectedLambdaExceptionError;

class DisabledUserError extends CognitoError {
  constructor() {
    super("DisabledUserException", "DisabledAccount");
  }

}

exports.DisabledUserError = DisabledUserError;

class InvalidParameterError extends CognitoError {
  constructor() {
    super("InvalidParameterException", "Invalid parameter");
  }

}

exports.InvalidParameterError = InvalidParameterError;

const unsupported = (message, res) => {
  console.error(`Cognito Local unsupported feature: ${message}`);
  return res.status(500).json({
    code: "CognitoLocal#Unsupported",
    message: `Cognito Local unsupported feature: ${message}`
  });
};

exports.unsupported = unsupported;