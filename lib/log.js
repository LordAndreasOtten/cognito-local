"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _debug = _interopRequireDefault(require("debug"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const logger = (0, _debug.default)("CognitoLocal");
var _default = {
  info: console.log,
  error: console.error,

  debug(...args) {
    if (logger.enabled) {
      logger.log(...args);
    }
  }

};
exports.default = _default;